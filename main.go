package main

import (
	"flag"
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

// KubeConfig is a dumping kubeconfig struct
type KubeConfig struct {
	APIVersion string `yaml:"apiVersion"`
	Clusters   []struct {
		Cluster struct {
			CertificateAuthorityData string `yaml:"certificate-authority-data"`
			Server                   string `yaml:"server"`
		} `yaml:"cluster"`
		Name string `yaml:"name"`
	} `yaml:"clusters"`
	Contexts []struct {
		Context struct {
			Cluster   string `yaml:"cluster"`
			Namespace string `yaml:"namespace"`
			User      string `yaml:"user"`
		} `yaml:"context,omitempty"`
		Name string `yaml:"name"`
	} `yaml:"contexts"`
	CurrentContext string `yaml:"current-context"`
	Kind           string `yaml:"kind"`
	Preferences    struct {
	} `yaml:"preferences"`
	Users []struct {
		Name string `yaml:"name"`
		User struct {
			Token string `yaml:"token"`
		} `yaml:"user"`
	} `yaml:"users"`
}

func main() {

	// Init flags
	flag.Parse()
	fileList := flag.Args()

	// Target struct
	mergedKC := KubeConfig{}

	for _, fPath := range fileList {

		// TODO: check if inarg is a path and file exists

		// Open given KubeConfig file
		f, err := os.Open(fPath)
		if err != nil {
			fmt.Printf("error: couln't open file %s", fileList[0])
		}
		defer f.Close()

		// Dump kubeconfig content into struct
		if len(mergedKC.Clusters) == 0 {
			mergedKC, err = loadKubeconfig(fPath)
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
			continue
		}
		kc, err := loadKubeconfig(fPath)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// TODO: check connectivity against fqdn, use it on success, use rancherms otherwise


		// TODO: append Clusters, Contexts and Users to main KubeConfig struct
		names := getKCNames(mergedKC, "clusters")
		for _, c := range kc.Clusters {
			if !arrayContains(names, c.Name) {
				mergedKC.Clusters = append(mergedKC.Clusters, c)
			}
		}

		names = getKCNames(mergedKC, "contexts")
		for _, c := range kc.Contexts {
			if !arrayContains(names, c.Name) {
				mergedKC.Contexts = append(mergedKC.Contexts, c)
			}
		}

		names = getKCNames(mergedKC, "users")
		for _, u := range kc.Users {
			if !arrayContains(names, u.Name) {
				mergedKC.Users = append(mergedKC.Users, u)
			}
		}

		// TODO: write main KubeConfig struct into output file
		f, err = os.Create("output.yaml")
		if err != nil {
			fmt.Println("error: cannot create output file")
		}
		err = yaml.NewEncoder(f).Encode(mergedKC)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// TODO: check connectivity against everycontext on the new file
	}

	
	fmt.Println(mergedKC.Clusters)

}

// ladKubeconfig gets a Kubeconfig file, reads it and dumps it into a KubeConfig struct
// which outputs as return
func loadKubeconfig(kubeconfig string) (KubeConfig, error) {

	kc := KubeConfig{}

	// Open YAML file
	file, err := os.Open(kubeconfig)
	if err != nil {
		panic(err.Error())
	}
	defer file.Close()

	// b , err := os.ReadFile(kubeconfig)
	// fmt.Println(string(b))

	// Decode YAML file to struct
	if file != nil {
		decoder := yaml.NewDecoder(file)
		if err := decoder.Decode(&kc); err != nil {
			return kc, fmt.Errorf("error decoding %s", file.Name())
		}
	}

	return kc, nil
}

// arrayContains checks wether a []string contains a specific string or not.
// Returns a boolean value
func arrayContains(l []string, s string) bool {

	for _, i := range l {
		if s == i {
			return true
		}
	}

	return false
}

// getClusterNames returns a []string containing the clusters' names of the
// given KubeConfig
func getKCNames(kc KubeConfig, kind string) []string {

	names := []string{}
	switch kind {
	case "clusters":
		list := kc.Clusters
		for _, u := range list {
			names = append(names, u.Name)
		}
	case "contexts":
		list := kc.Contexts
		for _, u := range list {
			names = append(names, u.Name)
		}
	case "users":
		list := kc.Users
		for _, u := range list {
			names = append(names, u.Name)
		}
	}

	return names
}
